import Model, { attr, hasMany } from '@ember-data/model';

export default class ParentModel extends Model {
  @attr name;
  @hasMany('child', { async: false }) children;
}
