import Model, { attr } from '@ember-data/model';

export default class ChildModel extends Model {
    @attr childName;
}
